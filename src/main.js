import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import AppDate from './components/AppDate.vue';
import store from './store';

const app = createApp(App)
app.use(router)
app.use(store)
app.component('AppDate', AppDate)
app.mount('#app')
