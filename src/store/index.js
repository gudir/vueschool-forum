import { createStore } from 'vuex'
import sourceData from '../data.json'

export default createStore({
    state: {
        ...sourceData,
        authId: 'ALXhxjwgY9PinwNGHpfai6OWyDu2'
    },
    getters: {
        authUser: state => {
            const user = state.users.find(user => user.id === state.authId)
            if(!user) return null

            return {
                ...user,
                get posts() {
                    return state.posts.filter(post => post.userId === user.id)
                },
                get postsCount() {
                    return this.posts.length
                },
                get threads() {
                    return state.threads.filter(thread => thread.userId === user.id)
                },
                get threadsCount() {
                    return this.threads.length
                }
            }
        }
    },
    actions: {
        createPost(context, post) {
            post.id = 'acsd'+Math.random();
            post.publishedAt = Math.floor(Date.now() / 1000),
            post.userId = this.state.authId,
            context.commit('setPost', { post }) //push ke array posts
            context.commit('appendPostToThread', { postId : post.id, threadId: post.threadId }) //push posts id ke thread
        },
        createThread({ commit, state }, { title, forumId }) {
            const thread = { 
                forumId,
                title,
                publishedAt: 1594035908,
                userId: this.state.authId,
                
                id: '-KsjWehQ--apjDBwSBCY'
            }
        },
        updateUser(context, user) {
            context.commit('setUser', { user, userId: user.id })
        }
    },
    mutations: {
        setPost(state, {post}) {
            state.posts.push(post)
        },
        appendPostToThread(state, { postId, threadId }) {
            const thread = state.threads.find(thread => thread.id === threadId);
            thread.posts.push(postId)
        },
        setUser(state, { user, userId }) {
            const userIndex = state.users.findIndex(user => user.id === userId);
            state.users[userIndex] = user;
        }
    }
});