import { createRouter, createWebHistory } from "vue-router";
import sourceData from "@/data.json";
import Home from '@/pages/Home.vue'
import ThreadShow from '@/pages/ThreadShow.vue';
import NotFound from '@/pages/NotFound.vue';
import Forum from '../pages/Forum.vue';
import Category from '../pages/Category.vue';
import Profile from '../pages/Profile.vue';

const routes = [
    { 
        path: '/',
        name: 'Home',
        component: Home 
    },
    {
        path: '/category/:id',
        name: 'Category',
        component: Category,
        props: true
    },
    { 
        path: '/forum/:id',
        name: 'Forum',
        component: Forum,
        props: true
    },
    { 
        path: '/profile',
        name: 'Profile',
        component: Profile,
        meta: { toTop: true, smoothScroll: true }
    },
    { 
        path: '/profile/edit',
        name: 'ProfileEdit',
        component: Profile,
        props: { edit: true }
    },
    { 
        path: '/thread/:id',
        name: 'ThreadShow',
        props: true,
        component: ThreadShow ,
        beforeEnter: (to, from, next) => {
            const threadExists = sourceData.threads.find(thread => thread.id === to.params.id);

            if(threadExists) {
                return next();
            }else{
                next({
                    name: 'NotFound',
                    params: { pathMatch: to.path.substring(1).split('/') },
                    // preserve existing query and hash
                    query: to.query,
                    hash: to.hash
                })
            }
        }
    },
    { 
        path: '/:pathMatch(.*)*', 
        name: 'NotFound', 
        component: NotFound 
    },
]

export default createRouter({
    //scroll to top
    scrollBehavior(to) {
        const scroll = {};
        if(to.meta.toTop) scroll.top = 0;
        if(to.meta.smoothScroll) scroll.behavior = 'smooth';
        return scroll;
    },
    // scrollBehavior(to, from, savedPosition) {
    //     if (savedPosition) {
    //         return savedPosition
    //     } else {
    //         return { top: 0 }
    //     }
    // },
    history: createWebHistory(),
    routes, // short for `routes: routes`
})